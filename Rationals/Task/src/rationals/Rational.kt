package rationals

import java.math.BigInteger
import java.math.BigInteger.ZERO

class Rational(numer: BigInteger, denum: BigInteger) : Comparable<Rational> {
    val numer: BigInteger
    val denum: BigInteger

    init {
        require(denum != ZERO) { "Denominator must be non-zero!" }
        val gcd = numer.gcd(denum)
        val sign = denum.signum().toBigInteger()
        this.numer = numer / gcd * sign
        this.denum = denum / gcd * sign
    }

    override fun compareTo(other: Rational): Int {
        return (this - other).numer.compareTo(ZERO)
    }

    /*override fun toString(): String {
        return if (denum == BigInteger.ONE) "$numer" else "$numer/$denum"
    }*/

    override fun toString(): String {
        val int = (numer / denum).abs().toInt()
        return when {
            denum == BigInteger.ONE -> "$numer"
            int == 0 -> "$numer/$denum"
            else -> "${numer.signum() * int}(${numer.abs() % denum}/$denum)"
        }
    }

    operator fun plus(other: Rational): Rational {
        val numerator = (numer * other.denum) + (other.numer * denum)
        val denominator = denum * other.denum
        return Rational(numerator, denominator)
    }

    operator fun minus(other: Rational): Rational {
        val numerator = (numer * other.denum) - (other.numer * denum)
        val denominator = denum * other.denum
        return Rational(numerator, denominator)
    }

    operator fun times(other: Rational): Rational {
        val numerator = numer * other.numer
        val denominator = denum * other.denum
        return Rational(numerator, denominator)
    }

    operator fun div(other: Rational): Rational {
        val numerator = numer * other.denum
        val denominator = denum * other.numer
        return Rational(numerator, denominator)
    }

    operator fun unaryMinus(): Rational = Rational(-numer, denum)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Rational

        if (numer != other.numer) return false
        if (denum != other.denum) return false

        return true
    }

    override fun hashCode(): Int {
        var result = numer.hashCode()
        result = 31 * result + denum.hashCode()
        return result
    }
}

fun String.toRational(): Rational {
    fun String.toBigIntegerOrFail(): BigInteger {
        return toBigIntegerOrNull() ?: throw IllegalArgumentException(
                """Expecting rational in the form of 'n/d' or n, 
                    |but got: '${this@toRational}'
                """.trimMargin()
        )
    }

    val pair = split("/")
    val numerator = pair[0].toBigIntegerOrFail()
    val denominator = if (pair.size == 2) pair[1].toBigIntegerOrFail() else BigInteger.ONE
    return Rational(numerator, denominator)
}

infix fun Number.divBy(denominator: Number): Rational =
        Rational(BigInteger.valueOf(this.toLong()), BigInteger.valueOf(denominator.toLong()))

fun main() {

    println("45".toRational() * "-12/35".toRational())

    /*val half = 1 divBy 2
    val third = 1 divBy 3

    val sum: Rational = half + third
    val rational = 5 divBy 6
    println(rational == sum)

    val difference: Rational = half - third
    println(1 divBy 6 == difference)

    val product: Rational = half * third
    println(1 divBy 6 == product)

    val quotient: Rational = half / third
    println(3 divBy 2 == quotient)

    val negation: Rational = -half
    println(-1 divBy 2 == negation)

    println((2 divBy 1).toString() == "2")
    println((-2 divBy 4).toString() == "-1/2")
    println("117/1098".toRational().toString() == "13/122")

    val twoThirds = 2 divBy 3
    println(half < twoThirds)

    println(half in third..twoThirds)

    println(2000000000L divBy 4000000000L == 1 divBy 2)

    println("912016490186296920119201192141970416029".toBigInteger() divBy
            "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)*/
}
